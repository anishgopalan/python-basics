import pandas as pd
from matplotlib import pyplot as plt
'''
x = [1, 2, 4, 2]
y = [1, 4, 1, 4]
s = [10,5,12,8]
plt.plot(x, y)
plt.plot(x,s)
plt.title("Test Chart")
plt.xlabel("X")
plt.ylabel("Y and S")
plt.legend(["This is Y","This is S" ])
#plt.show()
sample_data = pd.read_csv('sample_data.csv')
#print(sample_data)
#print(type(sample_data))
plt.plot(sample_data.column_a,sample_data.column_b)
#plt.show()
'''
data = pd.read_csv("countries.csv")
#print(data)
us = data[data.country == 'United States']
china = data[data.country == 'China']
print(us.population.iloc[0])# for retriving the first row population in matplotlib using pyplot
#print (us)
plt.plot(us.year, us.population/10**6)
plt.plot(china.year, china.population/10**6)

plt.xlabel("Year")
plt.ylabel("Population")
plt.legend(['United States', "China"])
plt.show()
