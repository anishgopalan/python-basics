
import sys
from cx_Freeze import setup,Executable
include_files = ['autorun.inf']
base = None

if sys.platform == "win32":
    base = "Win32GUI"
setup(
    name = "Anish",
    version = "0.1",
    description = "Computer Game",
    options = {'built_exe':{'include_files':include_files}},
    executables = [Executable("data visualisation.py",base=base)]
)
