class Robot:
    def __init__(self, n, c, w):
        self.name = n
        self.color = c
        self.weight = w


    def intrduce_self(self):
        print("My Name is " + self.name)  # this in java
        print("My Color is " + self.color)
        print("My Weight is", self.weight)

r1=Robot("Anish","Red",35)
r2 =Robot ("Kripa","Blue",30)

class Person:
    def __init__(self,n,p,i):
        self.name = n
        self.Personality = p
        self.isSitting = i

    def sit_Down(self):
        self.isSitting = True

    def stand_Up(self):
        self.isSitting = False


p1 = Person ("Anish","Brilliant",True)
p2 = Person ("Kripa","Silent",False)
#p1 owns r2 wiseverse
p1.robot_Owned =r2
p2.robot_Owned =r1

p1.robot_Owned.intrduce_self()