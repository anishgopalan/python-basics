a=["Anish ", "Kripa", "jagu", "jai", "Sreeraj"]
print(a)
for name in a:
    print(name)

b=(1,2,3,4,5,6,7,8)
print (b)
total=0

#loop Demo
for num in b:
    total=total+num
print (total)

#range demo
arr= list(range(1, 8))
print(arr)

#total of range
tot1=0
for i in range(1,5):
    tot1 += i
print(tot1)

#print only with condition from array
for arr1 in range(1, 10):
    if (arr1 % 3) == 0:
        print(arr1)