class Robot:
    '''
    def intrduce_self(self):
        print("My Name is " + self.name) # this in java
        print("My Color is " + self.color)
        print("My Weight is" , self.weight)
r1=Robot()
r1.name="Anish"
r1.color="Red"
r1.weight=45

r2=Robot()
r2.name="Kripa"
r2.color="Blue"
r2.weight=25

r1.intrduce_self()
r2.intrduce_self()
'''
# this code can be in different style using constructor

    def __init__(self,name,color,weight):
        self.name=name
        self.color=color
        self.weight=weight


    def intrduce_self(self):
         print("My Name is " + self.name)  # this in java
         print("My Color is " + self.color)
         print("My Weight is", self.weight)

r1 =Robot ("Anish","Red",45)
r2 =Robot ("Kripa","Blue",30)

r1.intrduce_self()
r2.intrduce_self()


