a=[1, 3, 5, 7, 9, 11]
print(a)
b = []
b.append(67)
b.append(58)
print(b)
# adding list items using for loop with components in a[] list C List
c=[]
for x in a:
    c.append(x*2)

print("C :" , c)

# Above List using Phyton list comprehensive method D List

d = [x * 2 for x in a]
print("D :" , d)
# list with 1 to 6 Squares
e = [x * x for x in(range(1,7))] # x*x is equal to x ** 2
print(e)

#create a list like [36,25,16,9,4,1]
f1 = [x * x for x in(range(6, 0, -1))]
print("f1 :", f1)

f2 =[]
for x in range(6, 0, -1):
    f2.append(x ** 2)

print("f2 :" , f2)