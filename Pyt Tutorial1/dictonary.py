d = {}
# d = {"Anish":32, "Kripa":24, "Jagu":12}
d["Anish"] = 32
d["Kripa"] = 24
d["Jagu"] = 12
#print(d["Anish"])
#d["Anish"] = 33
#print(d["Anish"])
#MIX KEYS LIKE STRING KEY AND INTEGER KEY IN ONE DICTONARY
#EG
d[12] = 890


for key,values in d.items():
    print("KEY : " , key,",   Values: ",values )
    print("")

    '''
    KEY: Anish, Values: 32

    KEY: Kripa, Values: 24

    KEY: Jagu, Values: 12

    KEY: 12, Values: 890

    Process
    finished
    with exit code 0
    '''