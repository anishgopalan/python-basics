print(type("Anish"))
print(type(54))
print(type(True))
#Boolean test Program
def are_you_Sad(is_Rainy,  has_Umberlla):
    if is_Rainy == True and  has_Umberlla == False:
        return True
    else:
        return False

print(are_you_Sad(True,True))

#another way to express above statement
def are_you_Sad1(is_Rainy,  has_Umberlla):
    if is_Rainy  and  not has_Umberlla :
        return True
    else:
        return False

print(are_you_Sad1(True,True))

def are_you_Sad2(is_Rainy,  has_Umberlla):
    return is_Rainy  and  not has_Umberlla
print(are_you_Sad2(True,False))

def c_greater_than_d_plus_e(c,d,e):
    return c > d+e
print (c_greater_than_d_plus_e(3,5,7))